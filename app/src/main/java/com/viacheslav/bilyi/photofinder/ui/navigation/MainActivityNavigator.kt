package com.viacheslav.bilyi.photofinder.ui.navigation

import com.viacheslav.bilyi.photofinder.ui.activity.MainActivity
import javax.inject.Inject

class MainActivityNavigator @Inject constructor(activity: MainActivity) : BaseNavigator(activity)
