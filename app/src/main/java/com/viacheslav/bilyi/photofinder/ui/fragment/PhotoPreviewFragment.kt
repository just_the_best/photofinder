package com.viacheslav.bilyi.photofinder.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.viacheslav.bilyi.photofinder.R
import com.viacheslav.bilyi.photofinder.domain.GetPhotoMetadataDescriptionInteractor
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoWithMetadata
import com.viacheslav.bilyi.photofinder.mvp.contract.PhotoPreviewContract
import com.viacheslav.bilyi.photofinder.ui.BaseFragment
import com.viacheslav.bilyi.photofinder.ui.MessageInterface
import javax.inject.Inject

class PhotoPreviewFragment :
    BaseFragment<PhotoPreviewContract.View, PhotoPreviewContract.Presenter>(),
    PhotoPreviewContract.View {

    override val layoutId = R.layout.fragment_photo_preview

    @Inject
    lateinit var messageInterface: MessageInterface

    @Inject
    lateinit var getPhotoMetadataDescription: GetPhotoMetadataDescriptionInteractor

    private lateinit var photoImageView: ImageView
    private lateinit var photoInfoImageView: ImageView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        photoImageView = view.findViewById(R.id.previewPhotoImageView)
        photoInfoImageView = view.findViewById(R.id.previewPhotoInfoImageView)

        val photoWithMetadata = requireNotNull(
            arguments?.getParcelable<PhotoWithMetadata>(PHOTO_WITH_METADATA_KEY)
        )

        val photo = requireNotNull(photoWithMetadata.photo)

        Glide.with(view)
            .load(photo.path)
            .into(photoImageView)

        photoInfoImageView.setOnClickListener {
            photoWithMetadata.metadata?.let {
                messageInterface.showInfo(message = getPhotoMetadataDescription(it))
            }
        }
    }

    companion object {

        private const val PHOTO_WITH_METADATA_KEY = "PHOTO_WITH_METADATA_KEY"

        fun newInstance(photoWithMetadata: PhotoWithMetadata) =
            PhotoPreviewFragment().apply {
                arguments = Bundle().also { bundle ->
                    bundle.putParcelable(PHOTO_WITH_METADATA_KEY, photoWithMetadata)
                }
            }
    }
}
