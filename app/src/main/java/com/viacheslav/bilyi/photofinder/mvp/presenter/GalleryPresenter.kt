package com.viacheslav.bilyi.photofinder.mvp.presenter

import com.tbruyelle.rxpermissions2.RxPermissions
import com.viacheslav.bilyi.photofinder.di.qualifier.Background
import com.viacheslav.bilyi.photofinder.di.qualifier.Foreground
import com.viacheslav.bilyi.photofinder.domain.PhotoFacade
import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoWithMetadata
import com.viacheslav.bilyi.photofinder.domain.entity.getPhotos
import com.viacheslav.bilyi.photofinder.mvp.BasePresenter
import com.viacheslav.bilyi.photofinder.mvp.contract.GalleryContract
import com.viacheslav.bilyi.photofinder.ui.navigation.Screens
import com.viacheslav.bilyi.photofinder.util.GALLERY_PERMISSIONS
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

class GalleryPresenter @Inject constructor(
    @Background backgroundScheduler: Scheduler,
    @Foreground foregroundScheduler: Scheduler,
    router: Router,
    private val rxPermissions: RxPermissions,
    private val photoFacade: PhotoFacade
) : BasePresenter<GalleryContract.View>(backgroundScheduler, foregroundScheduler, router),
    GalleryContract.Presenter {

    private var filteringByTextEnabled: Boolean = false
    private var filteringByFaceEnabled: Boolean = false
    private var searchQuery: String = ""

    private var photosWithMetadata: List<PhotoWithMetadata>? = null

    private var metadataUpdatingDisposable: Disposable? = null

    override fun attachView(view: GalleryContract.View) {
        super.attachView(view)

        requestGalleryPermissions()
    }

    override fun requestGalleryPermissions() {
        subscribe(rxPermissions.request(*GALLERY_PERMISSIONS)) { isGranted ->
            if (isGranted) {
                showPhotos()
            } else {
                view?.showPermissionNotGrantedMessage()
            }
        }
    }

    override fun updateFilteringPhotosByText(enabled: Boolean) {
        filteringByTextEnabled = enabled
        showPhotos()
    }

    override fun updateFilteringPhotosByFaceAvailability(enabled: Boolean) {
        filteringByFaceEnabled = enabled
        showPhotos()
    }

    override fun onPhotoSelected(photo: Photo) {
        findPhotoWithMetadata(photo).also {
            router.navigateTo(Screens.PhotoPreviewScreen(it))
        }
    }

    override fun onUpdateClicked() {
        if (metadataUpdatingDisposable == null) {

            view?.showMetadataUpdating(true)

            subscribe(
                upstream = photoFacade.updateMetadata(24) { showMetadataToPhotosRelation() },
                onComplete = {
                    metadataUpdatingDisposable = null
                    view?.showMetadataUpdating(false)
                    showPhotos()
                },
                disposableFunction = { metadataUpdatingDisposable = it }
            )
        } else {
            view?.showMetadataUpdating(false)

            metadataUpdatingDisposable?.dispose()
            metadataUpdatingDisposable = null
        }

        showMetadataToPhotosRelation()
    }

    override fun updateSearchQuery(query: String) {
        searchQuery = query
        showPhotos()
    }

    private fun showPhotos() {
        Timber.d("### show by $searchQuery $filteringByTextEnabled $filteringByFaceEnabled")
        subscribeWithProgress(
            upstream = photoFacade.requestPhotosWithMetadata(
                query = searchQuery,
                onlyWithText = filteringByTextEnabled,
                onlyWithFace = filteringByFaceEnabled
            ),
            onSuccess = {
                photosWithMetadata = it
                view?.showPhotos(it.getPhotos())
            },
            onError = { view?.showPermissionNotGrantedMessage() }
        )

        showMetadataToPhotosRelation()
    }

    private fun showMetadataToPhotosRelation() {
        subscribe(
            Single.zip(
                photoFacade.getMetadataQuantity(),
                photoFacade.getPhotosQuantity(),
                { metadataQuantity, photosQuantity -> metadataQuantity to photosQuantity }
            )
        ) { view?.showMetadataToPhotoRelation(it.first, it.second) }
    }

    private fun findPhotoWithMetadata(photo: Photo): PhotoWithMetadata =
        requireNotNull(photosWithMetadata?.find { it.photo == photo })
}