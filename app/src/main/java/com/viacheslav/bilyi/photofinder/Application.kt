package com.viacheslav.bilyi.photofinder

import com.viacheslav.bilyi.photofinder.di.component.DaggerAppComponent
import com.viacheslav.bilyi.photofinder.util.logger.CrashReportingTree
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber

class Application : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        setupTimber()
        setupRxJavaPlugins()
    }

    private fun setupTimber() = Timber.plant(CrashReportingTree(), Timber.DebugTree())

    private fun setupRxJavaPlugins() {
        RxJavaPlugins.setErrorHandler {
            @Suppress("ConstantConditionIf")
            if (BuildConfig.DEBUG) {
                Thread
                    .currentThread()
                    .uncaughtExceptionHandler
                    .uncaughtException(Thread.currentThread(), it)
            } else Timber.e(it, "Error without subscriber")
        }
    }

    override fun applicationInjector(): AndroidInjector<Application> =
        DaggerAppComponent.factory().create(this)
}