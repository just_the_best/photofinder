package com.viacheslav.bilyi.photofinder.data.ml.mapper

import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.FaceInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.FacesInfo
import javax.inject.Inject

class FirebaseVisionFacesToFaceInfoMapper @Inject constructor(
) : (List<FirebaseVisionFace>) -> FacesInfo {

    override fun invoke(firebaseFaces: List<FirebaseVisionFace>) =
        FacesInfo(
            faceInfoList = firebaseFaces
                .map {
                    FaceInfo(
                        isSmiling = it.smilingProbability > SMILING_PROBABILITY_THRESHOLD
                    )
                }
        )

    companion object {
        private const val SMILING_PROBABILITY_THRESHOLD = 0.5
    }
}