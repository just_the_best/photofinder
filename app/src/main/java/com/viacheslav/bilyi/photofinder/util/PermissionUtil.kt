package com.viacheslav.bilyi.photofinder.util

import android.Manifest

val GALLERY_PERMISSIONS: Array<String> = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)