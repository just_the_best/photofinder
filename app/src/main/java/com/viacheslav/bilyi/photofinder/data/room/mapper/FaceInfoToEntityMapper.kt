package com.viacheslav.bilyi.photofinder.data.room.mapper

import com.viacheslav.bilyi.photofinder.data.room.entity.FaceInfoEntity
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.FaceInfo
import javax.inject.Inject

class FaceInfoToEntityMapper @Inject constructor(
) : (FaceInfo, String) -> FaceInfoEntity {

    override fun invoke(faceInfo: FaceInfo, path: String) =
        FaceInfoEntity(
            photoPath = path,
            isSmiling = faceInfo.isSmiling
        )
}