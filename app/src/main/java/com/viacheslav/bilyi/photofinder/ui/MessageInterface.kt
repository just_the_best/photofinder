package com.viacheslav.bilyi.photofinder.ui

import android.app.Activity
import androidx.annotation.StringRes
import com.viacheslav.bilyi.photofinder.BuildConfig
import com.viacheslav.bilyi.photofinder.R
import javax.inject.Provider

abstract class MessageInterface(
    private val activityProvider: Provider<Activity>
) {

    fun showError(
        throwable: Throwable,
        @StringRes titleResId: Int = R.string.error_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = createMessageFromThrowable(throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    fun showInfo(
        message: String,
        throwable: Throwable? = null,
        @StringRes titleResId: Int = R.string.info_alert_title,
        @StringRes confirmButtonResId: Int? = R.string.ok,
        @StringRes cancelButtonResId: Int? = R.string.cancel,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    ) = activityProvider.get().let {
        showMessage(
            activity = it,
            message = buildMessage(message, throwable),
            title = it.getString(titleResId),
            confirmButtonResId = confirmButtonResId,
            cancelButtonResId = cancelButtonResId,
            confirmationListener = confirmationListener,
            cancelListener = cancelListener
        )
    }

    protected abstract fun showMessage(
        activity: Activity,
        message: String,
        title: String,
        @StringRes confirmButtonResId: Int?,
        @StringRes cancelButtonResId: Int?,
        confirmationListener: (() -> Unit)? = null,
        cancelListener: (() -> Unit)? = null
    )

    private fun createMessageFromThrowable(throwable: Throwable) = buildString {
        append(throwable::class.simpleName)
        throwable.message?.takeUnless(String::isEmpty)?.run { append(": $this") }
    }

    private fun buildMessage(message: String, throwable: Throwable?) = buildString {
        append(message)
        if (BuildConfig.DEBUG) {
            throwable?.let {
                append('\n')
                append(createMessageFromThrowable(it))
            }
        }
    }
}
