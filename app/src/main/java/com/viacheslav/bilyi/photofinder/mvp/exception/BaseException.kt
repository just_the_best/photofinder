package com.viacheslav.bilyi.photofinder.mvp.exception

open class BaseException(message: String? = null, cause: Throwable? = null) :
    RuntimeException(message, cause)
