package com.viacheslav.bilyi.photofinder.mvp.contract

import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.mvp.BaseContract

interface GalleryContract {

    interface View : BaseContract.View {

        fun showPhotos(photoList: List<Photo>)

        fun showPermissionNotGrantedMessage()

        fun showMetadataToPhotoRelation(
            metadataQuantity: Int,
            photoQuantity: Int
        )

        fun showMetadataUpdating(enable: Boolean)
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun requestGalleryPermissions()

        fun updateSearchQuery(query: String)

        fun updateFilteringPhotosByText(enabled: Boolean)

        fun updateFilteringPhotosByFaceAvailability(enabled: Boolean)

        fun onPhotoSelected(photo: Photo)

        fun onUpdateClicked()
    }
}