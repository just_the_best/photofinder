package com.viacheslav.bilyi.photofinder.di.module

import com.viacheslav.bilyi.photofinder.di.module.activity.MainActivityFragmentProvider
import com.viacheslav.bilyi.photofinder.di.module.activity.MainActivityModule
import com.viacheslav.bilyi.photofinder.di.module.activity.SplashActivityModule
import com.viacheslav.bilyi.photofinder.di.scope.ActivityScope
import com.viacheslav.bilyi.photofinder.ui.activity.MainActivity
import com.viacheslav.bilyi.photofinder.ui.activity.SplashActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesProvider {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            SplashActivityModule::class,
            NavigationModule::class,
            ExecutionModule::class
        ]
    )
    abstract fun contributeSplashActivityInjector(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            MainActivityFragmentProvider::class,
            MainActivityModule::class,
            NavigationModule::class,
            ExecutionModule::class
        ]
    )
    abstract fun contributeMainActivityInjector(): MainActivity
}
