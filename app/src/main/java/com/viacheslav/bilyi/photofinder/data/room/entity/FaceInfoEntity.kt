package com.viacheslav.bilyi.photofinder.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "face_info"
)
data class FaceInfoEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "face_info_id")
    val faceInfoId: Long = 0,

    @ColumnInfo(name = "parent_photo_path")
    val photoPath: String,

    @ColumnInfo(name = "is_smiling")
    val isSmiling: Boolean
)