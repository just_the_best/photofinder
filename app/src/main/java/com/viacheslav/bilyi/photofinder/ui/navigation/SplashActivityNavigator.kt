package com.viacheslav.bilyi.photofinder.ui.navigation

import com.viacheslav.bilyi.photofinder.ui.activity.SplashActivity
import javax.inject.Inject

class SplashActivityNavigator @Inject constructor(activity: SplashActivity) : BaseNavigator(activity)
