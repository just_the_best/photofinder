package com.viacheslav.bilyi.photofinder.di.module.activity

import android.app.Activity
import com.viacheslav.bilyi.photofinder.di.scope.ActivityScope
import com.viacheslav.bilyi.photofinder.mvp.contract.MainContract
import com.viacheslav.bilyi.photofinder.mvp.presenter.MainPresenter
import com.viacheslav.bilyi.photofinder.ui.MessageInterface
import com.viacheslav.bilyi.photofinder.ui.activity.MainActivity
import com.viacheslav.bilyi.photofinder.ui.dialog.AlertMessageInterface
import com.viacheslav.bilyi.photofinder.ui.navigation.BaseNavigator
import com.viacheslav.bilyi.photofinder.ui.navigation.MainActivityNavigator
import dagger.Binds
import dagger.Module

@Module
interface MainActivityModule {

    @Binds
    @ActivityScope
    fun bindAlertMessageInterface(messageInterface: AlertMessageInterface): MessageInterface

    @Binds
    @ActivityScope
    fun bindNavigator(navigator: MainActivityNavigator): BaseNavigator

    @Binds
    @ActivityScope
    fun bindPresenter(presenter: MainPresenter): MainContract.Presenter

    @Binds
    @ActivityScope
    fun bindActivity(activity: MainActivity): Activity
}
