package com.viacheslav.bilyi.photofinder.mvp

import androidx.annotation.CallSuper
import com.viacheslav.bilyi.photofinder.BuildConfig.DEBUG
import com.viacheslav.bilyi.photofinder.mvp.exception.ViewNotAttachedException
import io.reactivex.Scheduler
import ru.terrakok.cicerone.Router
import timber.log.Timber

abstract class BasePresenter<V : BaseContract.View> protected constructor(
    backgroundScheduler: Scheduler,
    foregroundScheduler: Scheduler,
    val router: Router
) : Execution(backgroundScheduler, foregroundScheduler),
    BaseContract.Presenter<V> {

    @Suppress("ConstantConditionIf")
    final override var view: V? = null
        get() = field ?: field.apply {
            if (DEBUG) throw ViewNotAttachedException() else Timber.e("View is not attached")
        }
        private set

    override fun onError(throwable: Throwable) {
        if (runCatching { view?.onError(throwable) }.getOrNull() == null) {
            Timber.e(throwable, "Can't show error, view is not attached")
        }
    }

    @CallSuper
    override fun attachView(view: V) {
        this.view = view
        createExecution()
    }

    @CallSuper
    override fun detachView() {
        destroyExecution()
        this.view = null
    }

    override fun showProgress() {
        view?.showProgress() ?: Timber.e("Can't show progress, view is not attached")
    }

    override fun hideProgress() {
        view?.hideProgress() ?: Timber.e("Can't hide progress, view is not attached")
    }
}