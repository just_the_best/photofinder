package com.viacheslav.bilyi.photofinder.ui.navigation


import android.content.Context
import android.content.Intent
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoWithMetadata
import com.viacheslav.bilyi.photofinder.ui.activity.MainActivity
import com.viacheslav.bilyi.photofinder.ui.fragment.GalleryFragment
import com.viacheslav.bilyi.photofinder.ui.fragment.PhotoPreviewFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {

    class MainScreen : SupportAppScreen() {

        override fun getActivityIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    class GalleryScreen : SupportAppScreen() {

        override fun getFragment() = GalleryFragment.newInstance()
    }

    class PhotoPreviewScreen(private val photoWithMetadata: PhotoWithMetadata) : SupportAppScreen() {

        override fun getFragment() = PhotoPreviewFragment.newInstance(photoWithMetadata)
    }

}
