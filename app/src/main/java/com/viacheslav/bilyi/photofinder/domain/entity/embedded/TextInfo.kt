package com.viacheslav.bilyi.photofinder.domain.entity.embedded

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TextInfo(
    val content: String?
) : Parcelable {

    fun isValid(): Boolean = content?.isNotEmpty() ?: false

    companion object {
        val EMPTY = TextInfo(null)
    }
}