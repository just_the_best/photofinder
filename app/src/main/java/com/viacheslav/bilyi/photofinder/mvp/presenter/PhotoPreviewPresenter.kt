package com.viacheslav.bilyi.photofinder.mvp.presenter

import com.viacheslav.bilyi.photofinder.di.qualifier.Background
import com.viacheslav.bilyi.photofinder.di.qualifier.Foreground
import com.viacheslav.bilyi.photofinder.mvp.BasePresenter
import com.viacheslav.bilyi.photofinder.mvp.contract.PhotoPreviewContract
import io.reactivex.Scheduler
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class PhotoPreviewPresenter @Inject constructor(
    @Background backgroundScheduler: Scheduler,
    @Foreground foregroundScheduler: Scheduler,
    router: Router
) : BasePresenter<PhotoPreviewContract.View>(backgroundScheduler, foregroundScheduler, router),
    PhotoPreviewContract.Presenter
