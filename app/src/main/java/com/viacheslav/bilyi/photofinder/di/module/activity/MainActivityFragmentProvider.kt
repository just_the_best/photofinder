package com.viacheslav.bilyi.photofinder.di.module.activity

import com.viacheslav.bilyi.photofinder.di.module.fragment.GalleryFragmentModule
import com.viacheslav.bilyi.photofinder.di.module.fragment.PhotoPreviewFragmentModule
import com.viacheslav.bilyi.photofinder.di.scope.FragmentScope
import com.viacheslav.bilyi.photofinder.ui.fragment.GalleryFragment
import com.viacheslav.bilyi.photofinder.ui.fragment.PhotoPreviewFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface MainActivityFragmentProvider {

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            GalleryFragmentModule::class
        ]
    )
    fun contributeMainFragmentInjector(): GalleryFragment

    @FragmentScope
    @ContributesAndroidInjector(
        modules = [
            PhotoPreviewFragmentModule::class
        ]
    )
    fun contributePhotoPreviewFragment(): PhotoPreviewFragment

}
