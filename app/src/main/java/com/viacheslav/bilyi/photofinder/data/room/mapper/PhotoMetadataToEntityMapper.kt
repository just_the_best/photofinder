package com.viacheslav.bilyi.photofinder.data.room.mapper

import com.viacheslav.bilyi.photofinder.data.room.entity.PhotoMetadataEntity
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import javax.inject.Inject

class PhotoMetadataToEntityMapper @Inject constructor(

) : (PhotoMetadata) -> PhotoMetadataEntity {

    override fun invoke(photoMetadata: PhotoMetadata) = PhotoMetadataEntity(
        photoPath = photoMetadata.path,
        textContent = photoMetadata.textInfo.content
    )
}