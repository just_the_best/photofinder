package com.viacheslav.bilyi.photofinder.mvp.contract

import com.viacheslav.bilyi.photofinder.mvp.BaseContract

interface SplashContract {

    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter<View> {

        fun onViewCreated()
    }
}
