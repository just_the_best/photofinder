package com.viacheslav.bilyi.photofinder.di.module.fragment

import com.tbruyelle.rxpermissions2.RxPermissions
import com.viacheslav.bilyi.photofinder.di.scope.FragmentScope
import com.viacheslav.bilyi.photofinder.mvp.contract.GalleryContract
import com.viacheslav.bilyi.photofinder.mvp.presenter.GalleryPresenter
import com.viacheslav.bilyi.photofinder.ui.fragment.GalleryFragment
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface GalleryFragmentModule {

    @Binds
    @FragmentScope
    fun bindPresenter(presenter: GalleryPresenter): GalleryContract.Presenter

    @Module
    companion object {

        @JvmStatic
        @Provides
        @FragmentScope
        fun rxPermissions(fragment: GalleryFragment) = RxPermissions(fragment)
    }
}
