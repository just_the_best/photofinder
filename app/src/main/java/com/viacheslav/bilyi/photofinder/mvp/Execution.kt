package com.viacheslav.bilyi.photofinder.mvp

import androidx.annotation.CallSuper
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

abstract class Execution(
    private val backgroundScheduler: Scheduler,
    private val foregroundScheduler: Scheduler
) {

    private var compositeDisposable: CompositeDisposable? = null

    @CallSuper
    protected open fun createExecution() {
        if (isDisposed()) {
            compositeDisposable = CompositeDisposable()
        }
    }

    @CallSuper
    protected open fun destroyExecution() {
        compositeDisposable?.dispose()
    }

    protected fun <T : Any> subscribe(upstream: Observable<T>, onNext: (T) -> Unit) {
        if (!isDisposed()) {
            upstream
                .subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler)
                .subscribeBy(onError = this::onError, onNext = onNext)
                .apply(this::addDisposable)
        }
    }

    protected fun <T : Any> subscribe(upstream: Single<T>, onSuccess: (T) -> Unit) {
        if (!isDisposed()) {
            upstream
                .subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler)
                .subscribeBy(this::onError, onSuccess)
                .apply(this::addDisposable)
        }
    }

    protected fun subscribe(upstream: Completable, onComplete: () -> Unit) {
        if (!isDisposed()) {
            upstream
                .subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler)
                .subscribeBy(this::onError, onComplete)
                .apply(this::addDisposable)
        }
    }

    protected fun subscribe(
        upstream: Completable,
        onComplete: () -> Unit,
        disposableFunction: (Disposable) -> Unit
    ) {
        if (!isDisposed()) {
            upstream
                .subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler)
                .subscribeBy(this::onError, onComplete)
                .apply(this::addDisposable)
                .also(disposableFunction)
        }
    }

    protected fun <T : Any> subscribeWithProgress(
        upstream: Observable<T>,
        onNext: (T) -> Unit,
        showProgress: (Disposable) -> Unit = { showProgress() },
        hideProgress: () -> Unit = this::hideProgress
    ) {
        if (!isDisposed()) {
            upstream
                .subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler)
                .doOnSubscribe(showProgress)
                .doAfterTerminate(hideProgress)
                .subscribeBy(onError = this::onError, onNext = onNext)
                .apply(this::addDisposable)
        }
    }

    protected fun <T : Any> subscribeWithProgress(
        upstream: Single<T>,
        onSuccess: (T) -> Unit,
        onError: (Throwable) -> Unit = this::onError,
        showProgress: (Disposable) -> Unit = { showProgress() },
        hideProgress: () -> Unit = this::hideProgress
    ) {
        if (!isDisposed()) {
            upstream
                .subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler)
                .doOnSubscribe(showProgress)
                .doAfterTerminate(hideProgress)
                .subscribeBy(onError, onSuccess)
                .apply(this::addDisposable)
        }
    }

    protected fun subscribeWithProgress(
        upstream: Completable,
        onComplete: () -> Unit,
        onError: (Throwable) -> Unit = this::onError,
        showProgress: (Disposable) -> Unit = { showProgress() },
        hideProgress: () -> Unit = this::hideProgress
    ) {
        if (!isDisposed()) {
            upstream
                .subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler)
                .doOnSubscribe(showProgress)
                .doAfterTerminate(hideProgress)
                .subscribeBy(onError, onComplete)
                .apply(this::addDisposable)
        }
    }

    protected abstract fun onError(throwable: Throwable)

    protected abstract fun showProgress()

    protected abstract fun hideProgress()

    private fun addDisposable(disposable: Disposable) {
        if (!isDisposed()) compositeDisposable?.add(disposable)
    }

    private fun isDisposed(): Boolean {
        if (compositeDisposable == null) {
            Timber.d("CompositeDisposable is null, action shouldn't start")
            return true
        }

        if (compositeDisposable?.isDisposed == true) {
            Timber.d("CompositeDisposable is disposed, action shouldn't start")
            return true
        }

        return false
    }
}
