package com.viacheslav.bilyi.photofinder.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.viacheslav.bilyi.photofinder.data.room.dao.PhotoMetadataDao
import com.viacheslav.bilyi.photofinder.data.room.entity.FaceInfoEntity
import com.viacheslav.bilyi.photofinder.data.room.entity.LabelInfoEntity
import com.viacheslav.bilyi.photofinder.data.room.entity.PhotoMetadataEntity

@Database(
    entities = [
        PhotoMetadataEntity::class,
        FaceInfoEntity::class,
        LabelInfoEntity::class
    ],
    version = PhotoMetadataDatabase.VERSION
)
abstract class PhotoMetadataDatabase : RoomDatabase() {

    abstract fun getPhotoMetadataDao(): PhotoMetadataDao

    companion object {

        const val VERSION = 1
        const val NAME = "PhotoMetaDatabase"
    }
}