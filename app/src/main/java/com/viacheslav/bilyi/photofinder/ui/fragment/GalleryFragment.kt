package com.viacheslav.bilyi.photofinder.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.viacheslav.bilyi.photofinder.R
import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.mvp.contract.GalleryContract
import com.viacheslav.bilyi.photofinder.ui.BaseFragment
import com.viacheslav.bilyi.photofinder.ui.MessageInterface
import com.viacheslav.bilyi.photofinder.ui.adapter.PhotoRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_gallery.*
import javax.inject.Inject

class GalleryFragment :
    BaseFragment<GalleryContract.View, GalleryContract.Presenter>(),
    GalleryContract.View {

    override val layoutId = R.layout.fragment_gallery

    @Inject
    lateinit var messageInterface: MessageInterface

    private val photoRecyclerAdapter = PhotoRecyclerAdapter { photo ->
        presenter.onPhotoSelected(photo)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<RecyclerView>(R.id.photosRecyclerView)
            .also {
                it.layoutManager =
                    GridLayoutManager(
                        requireContext(),
                        resources.getInteger(R.integer.gallery_screen_column_count)
                    )
                it.adapter = photoRecyclerAdapter
            }

        view.findViewById<EditText>(R.id.findQueryEditText)
            .addTextChangedListener { queryEditable ->
                presenter.updateSearchQuery(queryEditable.toString())
            }

        view.findViewById<CheckBox>(R.id.textFilterCheckBox)
            .setOnCheckedChangeListener { _, isChecked ->
                presenter.updateFilteringPhotosByText(isChecked)
            }


        view.findViewById<CheckBox>(R.id.facesFilterCheckBox)
            .setOnCheckedChangeListener { _, isChecked ->
                presenter.updateFilteringPhotosByFaceAvailability(isChecked)
            }

        updatingMetadataImageView.setOnClickListener {
            presenter.onUpdateClicked()
        }
    }

    override fun showPhotos(photoList: List<Photo>) {
        photoRecyclerAdapter.replaceItems(photoList)
    }

    override fun showPermissionNotGrantedMessage() {
        messageInterface
            .showInfo(
                message = resources.getString(R.string.gallery_screen_permissions_not_granted_message),
                confirmationListener = { presenter.requestGalleryPermissions() },
                cancelButtonResId = null
            )
    }

    override fun showMetadataToPhotoRelation(
        metadataQuantity: Int,
        photoQuantity: Int
    ) {
        metadataToPhotoRelationTextView.text = resources.getString(
            R.string.gallery_screen_metadata_to_photos_relation,
            metadataQuantity,
            photoQuantity
        )
    }

    override fun showMetadataUpdating(enable: Boolean) {
        updatingMetadataImageView.setImageResource(
            if (enable) R.drawable.ic_stop
            else R.drawable.ic_update
        )
    }

    companion object {

        fun newInstance() = GalleryFragment()
    }
}
