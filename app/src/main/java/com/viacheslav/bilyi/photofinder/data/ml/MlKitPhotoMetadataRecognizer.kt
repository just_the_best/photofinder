package com.viacheslav.bilyi.photofinder.data.ml

import android.content.Context
import android.net.Uri
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import com.viacheslav.bilyi.photofinder.data.ml.mapper.FirebaseVisionFacesToFaceInfoMapper
import com.viacheslav.bilyi.photofinder.data.ml.mapper.FirebaseVisionImageLabelsToLabelInfoMapper
import com.viacheslav.bilyi.photofinder.data.ml.mapper.FirebaseVisionTextToTextInfoMapper
import com.viacheslav.bilyi.photofinder.domain.PhotoMetadataRecognizer
import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.FacesInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.LabelsInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.TextInfo
import io.reactivex.Single
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class MlKitPhotoMetadataRecognizer @Inject constructor(
    private val context: Context,
    private val textRecognizer: FirebaseVisionTextRecognizer,
    private val visionTextToTextInfoMapper: FirebaseVisionTextToTextInfoMapper,
    private val visionFacesToFaceInfoMapper: FirebaseVisionFacesToFaceInfoMapper,
    private val firebaseLabelsMapper: FirebaseVisionImageLabelsToLabelInfoMapper
) : PhotoMetadataRecognizer {

    private val faceDetector =
        FirebaseVision
            .getInstance()
            .getVisionFaceDetector(getFaceOptions())

    private val labeler =
        FirebaseVision
            .getInstance()
            .onDeviceImageLabeler

    override fun recognizeMetadata(photo: Photo): Single<PhotoMetadata> {
        val visionImage = FirebaseVisionImage.fromFilePath(context, Uri.fromFile(File(photo.path)))

        return Single.zip(
            recognizeTextInfo(visionImage),
            recognizeLabels(visionImage),
            recognizeFaceInfo(visionImage),
            { textInfo, labels, faceInfo ->
                PhotoMetadata(
                    path = photo.path,
                    textInfo = textInfo,
                    labelsInfo = labels,
                    faceInfo = faceInfo
                ).also {
                    Timber.d("### recognized $photo = $it ")
                }
            }
        )
    }

    private fun recognizeTextInfo(visionImage: FirebaseVisionImage): Single<TextInfo> =
        Single.create { emitter ->
            textRecognizer
                .processImage(visionImage)
                .addOnSuccessListener { emitter.onSuccess(visionTextToTextInfoMapper(it)) }
                .addOnFailureListener(emitter::onError)
        }

    private fun recognizeLabels(visionImage: FirebaseVisionImage): Single<LabelsInfo> =
        Single.create { emitter ->
            labeler
                .processImage(visionImage)
                .addOnSuccessListener { emitter.onSuccess(firebaseLabelsMapper(it)) }
                .addOnFailureListener(emitter::onError)
        }

    private fun recognizeFaceInfo(visionImage: FirebaseVisionImage): Single<FacesInfo> =
        Single.create { emitter ->
            faceDetector
                .detectInImage(visionImage)
                .addOnSuccessListener { emitter.onSuccess(visionFacesToFaceInfoMapper(it)) }
                .addOnFailureListener(emitter::onError)
        }


    private fun getFaceOptions(): FirebaseVisionFaceDetectorOptions =
        FirebaseVisionFaceDetectorOptions.Builder()
            .setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
            .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
            .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
            .build()
}