package com.viacheslav.bilyi.photofinder.domain.entity.embedded

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LabelInfo(
    val text: String
) : Parcelable