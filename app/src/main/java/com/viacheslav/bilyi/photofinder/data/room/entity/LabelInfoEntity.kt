package com.viacheslav.bilyi.photofinder.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "label_info"
)
data class LabelInfoEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "label_info_id")
    val faceInfoId: Long = 0,

    @ColumnInfo(name = "parent_photo_path")
    val photoPath: String,

    @ColumnInfo(name = "label_text")
    val text: String
)