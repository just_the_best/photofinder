package com.viacheslav.bilyi.photofinder.domain

import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import io.reactivex.Single

interface PhotoRepository {

    fun getAll(): Single<List<Photo>>

    fun getAllSortedByPhotoPath(): Single<List<Photo>>
}