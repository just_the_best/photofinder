package com.viacheslav.bilyi.photofinder.domain

import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import io.reactivex.Completable
import io.reactivex.Single

interface PhotoMetadataRepository {

    fun addPhotoMetadata(photoMetadata: PhotoMetadata): Completable

    fun removePhotoMetadata(photoMetadata: PhotoMetadata): Completable

    fun getPhotoMetadata(photo: Photo): Single<PhotoMetadata>

    fun getAllMetadata(): Single<List<PhotoMetadata>>

    fun getAllMetadataSortedByPhotoPath(): Single<List<PhotoMetadata>>
}