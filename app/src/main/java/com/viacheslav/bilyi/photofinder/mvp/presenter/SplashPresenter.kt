package com.viacheslav.bilyi.photofinder.mvp.presenter

import com.viacheslav.bilyi.photofinder.di.qualifier.Background
import com.viacheslav.bilyi.photofinder.di.qualifier.Foreground
import com.viacheslav.bilyi.photofinder.domain.PhotoFacade
import com.viacheslav.bilyi.photofinder.mvp.BasePresenter
import com.viacheslav.bilyi.photofinder.mvp.contract.SplashContract
import com.viacheslav.bilyi.photofinder.ui.navigation.Screens
import io.reactivex.Scheduler
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

class SplashPresenter @Inject constructor(
    @Background backgroundScheduler: Scheduler,
    @Foreground foregroundScheduler: Scheduler,
    router: Router,
    private val photoFacade: PhotoFacade
) : BasePresenter<SplashContract.View>(backgroundScheduler, foregroundScheduler, router),
    SplashContract.Presenter {


    override fun onViewCreated() {
        subscribeWithProgress(
            upstream = photoFacade.updateMetadata(0),
            onComplete = { navigateToTargetScreen() },
            onError = {
                Timber.e(it)
                navigateToTargetScreen()
            }
        )
    }

    private fun navigateToTargetScreen() {
        router.newRootScreen(Screens.MainScreen())
    }
}
