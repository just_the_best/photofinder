package com.viacheslav.bilyi.photofinder.ui.activity

import android.os.Bundle
import com.viacheslav.bilyi.photofinder.R
import com.viacheslav.bilyi.photofinder.mvp.contract.SplashContract
import com.viacheslav.bilyi.photofinder.ui.BaseActivity

class SplashActivity : BaseActivity<SplashContract.View, SplashContract.Presenter>(),
    SplashContract.View {

    override val layoutId = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onViewCreated()
    }
}