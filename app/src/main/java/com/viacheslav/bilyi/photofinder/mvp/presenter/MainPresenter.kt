package com.viacheslav.bilyi.photofinder.mvp.presenter

import com.viacheslav.bilyi.photofinder.di.qualifier.Background
import com.viacheslav.bilyi.photofinder.di.qualifier.Foreground
import com.viacheslav.bilyi.photofinder.mvp.BasePresenter
import com.viacheslav.bilyi.photofinder.mvp.contract.MainContract
import com.viacheslav.bilyi.photofinder.ui.navigation.Screens
import io.reactivex.Scheduler
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MainPresenter @Inject constructor(
    @Background backgroundScheduler: Scheduler,
    @Foreground foregroundScheduler: Scheduler,
    router: Router
) : BasePresenter<MainContract.View>(backgroundScheduler, foregroundScheduler, router),
    MainContract.Presenter {

    override fun showSplashScreen() = router.newRootScreen(Screens.GalleryScreen())
}