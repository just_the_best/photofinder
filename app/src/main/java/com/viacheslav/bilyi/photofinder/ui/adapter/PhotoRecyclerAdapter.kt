package com.viacheslav.bilyi.photofinder.ui.adapter

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.viacheslav.bilyi.photofinder.R
import com.viacheslav.bilyi.photofinder.domain.entity.Photo

class PhotoRecyclerAdapter(
    private val onPhotoClickListener: (Photo) -> Unit
) : BaseAdapter<Photo>() {

    override fun getLayoutId(viewType: Int) = R.layout.item_photo

    override fun onCreateViewHolder(view: View, viewType: Int): BaseViewHolder<Photo> =
        PhotoViewHolder(view, onPhotoClickListener)
}

class PhotoViewHolder(
    itemView: View,
    private val onPhotoClickListener: (Photo) -> Unit

) : BaseViewHolder<Photo>(itemView) {

    private val photoImageView: ImageView = itemView.findViewById(R.id.photoImageView)

    init {
        itemView.setOnClickListener {
            onPhotoClickListener(item)
        }
    }

    override fun bind(item: Photo) {
        super.bind(item)

        Glide.with(photoImageView)
            .load(item.path)
            .into(photoImageView)
    }
}

