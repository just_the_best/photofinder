package com.viacheslav.bilyi.photofinder.data.room.mapper

import com.viacheslav.bilyi.photofinder.data.room.entity.FaceInfoEntity
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.FaceInfo
import javax.inject.Inject

class FaceInfoEntityToDomainMapper @Inject constructor(

) : (FaceInfoEntity) -> FaceInfo {

    override fun invoke(entity: FaceInfoEntity) =
        FaceInfo(
            isSmiling = entity.isSmiling
        )
}