package com.viacheslav.bilyi.photofinder.data.room.mapper

import com.viacheslav.bilyi.photofinder.data.room.entity.RoomPhotoMetadata
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.FacesInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.LabelsInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.TextInfo
import javax.inject.Inject

class RoomPhotoMetadataToDomainMapper @Inject constructor(
    private val faceInfoEntityToDomainMapper: FaceInfoEntityToDomainMapper,
    private val labelInfoEntityToDomainMapper: LabelInfoEntityToDomainMapper
) : (RoomPhotoMetadata) -> PhotoMetadata {

    override fun invoke(roomPhotoMetadata: RoomPhotoMetadata) =
        PhotoMetadata(
            path = roomPhotoMetadata.metadataEntity.photoPath,
            textInfo = TextInfo(roomPhotoMetadata.metadataEntity.textContent),
            labelsInfo = LabelsInfo(
                labels = roomPhotoMetadata.labelInfoEntityList.map(labelInfoEntityToDomainMapper)
            ),
            faceInfo = FacesInfo(
                faceInfoList = roomPhotoMetadata.faceInfoEntityList.map(faceInfoEntityToDomainMapper)
            )
        )
}