package com.viacheslav.bilyi.photofinder.di.module

import android.content.ContentResolver
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun providesContentResolver(context: Context): ContentResolver =
        context.contentResolver
}