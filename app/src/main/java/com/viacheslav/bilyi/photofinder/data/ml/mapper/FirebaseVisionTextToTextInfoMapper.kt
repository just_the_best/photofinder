package com.viacheslav.bilyi.photofinder.data.ml.mapper

import com.google.firebase.ml.vision.text.FirebaseVisionText
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.TextInfo
import com.viacheslav.bilyi.photofinder.util.concatenate
import javax.inject.Inject

class FirebaseVisionTextToTextInfoMapper @Inject constructor(
) : (FirebaseVisionText) -> TextInfo {

    override fun invoke(visionText: FirebaseVisionText) =
        TextInfo(
            content = visionText.textBlocks.map { it.text }.concatenate()
        )

}