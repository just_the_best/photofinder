package com.viacheslav.bilyi.photofinder.data.room.mapper

import com.viacheslav.bilyi.photofinder.data.room.entity.LabelInfoEntity
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.LabelInfo
import javax.inject.Inject

class LabelInfoToEntityMapper @Inject constructor(

) : (LabelInfo, String) -> LabelInfoEntity {

    override fun invoke(labelInfo: LabelInfo, path: String) =
        LabelInfoEntity(
            photoPath = path,
            text = labelInfo.text
        )
}