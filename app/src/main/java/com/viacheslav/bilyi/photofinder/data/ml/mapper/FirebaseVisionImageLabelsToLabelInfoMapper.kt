package com.viacheslav.bilyi.photofinder.data.ml.mapper

import com.google.firebase.ml.vision.label.FirebaseVisionImageLabel
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.LabelInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.LabelsInfo
import javax.inject.Inject

class FirebaseVisionImageLabelsToLabelInfoMapper @Inject constructor(
) : (List<FirebaseVisionImageLabel>) -> LabelsInfo {

    override fun invoke(firebaseLabels: List<FirebaseVisionImageLabel>) =
        LabelsInfo(
            labels = firebaseLabels.map {
                LabelInfo(
                    text = it.text)
            }
        )
}