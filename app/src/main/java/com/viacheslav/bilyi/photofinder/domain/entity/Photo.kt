package com.viacheslav.bilyi.photofinder.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Photo(
    val path: String
) : Parcelable