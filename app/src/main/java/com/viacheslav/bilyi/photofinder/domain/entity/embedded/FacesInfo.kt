package com.viacheslav.bilyi.photofinder.domain.entity.embedded

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FacesInfo(
    val faceInfoList: List<FaceInfo>
) : Parcelable {

    companion object {
        val EMPTY = FacesInfo(emptyList())
    }
}