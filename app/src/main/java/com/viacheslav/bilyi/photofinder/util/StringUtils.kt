package com.viacheslav.bilyi.photofinder.util

fun <R> List<R>.concatenate(): String =
    StringBuilder().also { builder ->
        this.forEach { builder.append(it) }
    }.toString()