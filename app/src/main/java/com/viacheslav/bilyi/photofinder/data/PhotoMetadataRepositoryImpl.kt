package com.viacheslav.bilyi.photofinder.data

import com.viacheslav.bilyi.photofinder.data.room.dao.PhotoMetadataDao
import com.viacheslav.bilyi.photofinder.data.room.mapper.PhotoMetadataToRoomMapper
import com.viacheslav.bilyi.photofinder.data.room.mapper.RoomPhotoMetadataToDomainMapper
import com.viacheslav.bilyi.photofinder.domain.PhotoMetadataRepository
import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PhotoMetadataRepositoryImpl @Inject constructor(
    private val photoMetadataDao: PhotoMetadataDao,
    private val roomMetadataToDomainMapperRoom: RoomPhotoMetadataToDomainMapper,
    private val metadataToRoomMapper: PhotoMetadataToRoomMapper
) : PhotoMetadataRepository {

    override fun addPhotoMetadata(photoMetadata: PhotoMetadata): Completable =
        photoMetadataDao
            .insert(metadataToRoomMapper(photoMetadata))
            .subscribeOn(Schedulers.io())

    override fun removePhotoMetadata(photoMetadata: PhotoMetadata): Completable {
        TODO("implement")
    }

    override fun getPhotoMetadata(photo: Photo): Single<PhotoMetadata> {
        TODO("implement")
    }

    override fun getAllMetadata(): Single<List<PhotoMetadata>> =
        photoMetadataDao.getAll()
            .map { list -> list.map { roomMetadataToDomainMapperRoom(it) } }
            .subscribeOn(Schedulers.io())

    override fun getAllMetadataSortedByPhotoPath(): Single<List<PhotoMetadata>> =
        photoMetadataDao.getAllSorted()
            .map { list -> list.map { roomMetadataToDomainMapperRoom(it) } }
            .subscribeOn(Schedulers.io())
}