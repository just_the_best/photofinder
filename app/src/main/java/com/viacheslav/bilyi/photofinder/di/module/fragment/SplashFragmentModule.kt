package com.viacheslav.bilyi.photofinder.di.module.fragment

import com.viacheslav.bilyi.photofinder.di.scope.FragmentScope
import com.viacheslav.bilyi.photofinder.mvp.contract.SplashContract
import com.viacheslav.bilyi.photofinder.mvp.presenter.SplashPresenter
import dagger.Binds
import dagger.Module

@Module
interface SplashFragmentModule {

    @Binds
    @FragmentScope
    fun bindPresenter(presenter: SplashPresenter): SplashContract.Presenter
}
