package com.viacheslav.bilyi.photofinder.data.content

import android.content.ContentResolver
import android.content.Context
import android.content.pm.PackageManager
import android.provider.MediaStore
import androidx.core.content.ContextCompat
import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import io.reactivex.Single
import javax.inject.Inject

class ContentResolverDao @Inject constructor(
    private val contentResolver: ContentResolver,
    private val context: Context
) {

    fun getAll(): Single<List<Photo>> =
        Single.fromCallable {
            if (isPermissionsGranted()) {
                contentResolver.query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    arrayOf(MediaStore.Images.Media.DATA),
                    null,
                    null,
                    null
                )?.use { cursor ->
                    val photoPathList = mutableListOf<Photo>()

                    val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                    while (cursor.moveToNext()) {
                        photoPathList += Photo(
                            path = cursor.getString(columnIndex)
                        )
                    }

                    photoPathList
                }
            } else {
                emptyList()
            }
        }.map { it.reversed() }


    private fun isPermissionsGranted(): Boolean =
        ContextCompat.checkSelfPermission(
            context,
            android.Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
}