package com.viacheslav.bilyi.photofinder.di.module

import com.viacheslav.bilyi.photofinder.di.qualifier.Background
import com.viacheslav.bilyi.photofinder.di.qualifier.Foreground
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
object ExecutionModule {

    @Provides
    @JvmStatic
    @Background
    fun provideBackgroundScheduler(): Scheduler = Schedulers.computation()

    @Provides
    @JvmStatic
    @Foreground
    fun provideForegroundScheduler(): Scheduler = AndroidSchedulers.mainThread()
}
