package com.viacheslav.bilyi.photofinder.mvp

interface BaseContract {

    interface View {

        fun onError(throwable: Throwable)

        fun showProgress()

        fun hideProgress()
    }

    interface Presenter<V : View> {

        val view: V?

        fun onError(throwable: Throwable)

        fun attachView(view: V)

        fun detachView()
    }
}
