package com.viacheslav.bilyi.photofinder.di.module.data

import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import com.viacheslav.bilyi.photofinder.data.ml.MlKitPhotoMetadataRecognizer
import com.viacheslav.bilyi.photofinder.domain.PhotoMetadataRecognizer
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
interface MlKitModule {

    @Binds
    @Singleton
    fun bindsPhotoMetadataRecognizer(impl: MlKitPhotoMetadataRecognizer): PhotoMetadataRecognizer

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        fun firebaseVisionTextRecognizer(): FirebaseVisionTextRecognizer =
            FirebaseVision
                .getInstance()
                .onDeviceTextRecognizer
    }
}