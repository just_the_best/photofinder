package com.viacheslav.bilyi.photofinder.domain

import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoWithMetadata
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class PhotoFacade @Inject constructor(
    private val photoRepository: PhotoRepository,
    private val photoMetadataRecognizer: PhotoMetadataRecognizer,
    private val photoMetadataRepository: PhotoMetadataRepository,
    private val getPhotoMetadataDescription: GetPhotoMetadataDescriptionInteractor
) {

    fun requestPhotosWithMetadata(
        query: String,
        onlyWithText: Boolean,
        onlyWithFace: Boolean
    ): Single<List<PhotoWithMetadata>> =
        getAllPhotos()
            .filterIfEnabled(onlyWithText) {
                it.metadata?.textInfo?.isValid() ?: false
            }
            .filterIfEnabled(onlyWithFace) {
                it.metadata?.faceInfo?.faceInfoList?.isNotEmpty() ?: false
            }
            .filterIfEnabled(query.isNotBlank()) {
                if (it.metadata == null) false
                else getPhotoMetadataDescription(it.metadata).contains(query.toLowerCase())
            }

    private fun <T> Single<List<T>>.filterIfEnabled(
        enabled: Boolean,
        predicate: (T) -> Boolean
    ): Single<List<T>> =
        if (enabled) this.map { it.filter(predicate) }
        else this

    fun updateMetadata(
        photosQuantity: Int = Int.MAX_VALUE,
        onUpdated: ((PhotoMetadata) -> Unit)? = null
    ): Completable =
        getPhotosWithoutMetadata(photosQuantity)
            .flatMapCompletable { photos ->
                photos
                    .map { recognizeAndSave(it, onUpdated) }
                    .toTypedArray()
                    .let { Completable.mergeArray(*it) }
            }

    private fun recognizeAndSave(
        photo: Photo,
        onUpdated: ((PhotoMetadata) -> Unit)? = null
    ): Completable =
        photoMetadataRecognizer
            .recognizeMetadata(photo)
            .flatMapCompletable { metadata ->
                photoMetadataRepository
                    .addPhotoMetadata(metadata)
                    .doOnComplete { onUpdated?.invoke(metadata) }
            }

    fun getPhotosQuantity(): Single<Int> =
        photoRepository
            .getAll()
            .map { it.size }

    fun getMetadataQuantity(): Single<Int> =
        photoMetadataRepository
            .getAllMetadata()
            .map { it.size }

    private fun getPhotosWithoutMetadata(photosQuantity: Int): Single<List<Photo>> =
        Single.zip(
            photoRepository.getAll(),
            photoMetadataRepository.getAllMetadata(),
            { photoList, photoMetadataList ->
                val photosWithoutMetadata = mutableListOf<Photo>()
                val photosWithMetadataPathSet = photoMetadataList.map { it.path }.toSet()

                photoList.forEach {
                    if (!photosWithMetadataPathSet.contains(it.path)) {
                        photosWithoutMetadata += it
                    }
                }

                photosWithoutMetadata.take(photosQuantity)
            }
        )

    /**
     * @return matched all photos and all PhotoMetadata. If photo without metadata or vice versa,
     * will be [PhotoWithMetadata] with null property
     */
    private fun getAllPhotos(): Single<List<PhotoWithMetadata>> =
        Single.zip(
            photoRepository.getAllSortedByPhotoPath(),
            photoMetadataRepository.getAllMetadataSortedByPhotoPath(),
            { photoList, metadataList ->

                val photoWithMetadataList = mutableListOf<PhotoWithMetadata>()

                var photoI = 0
                var metadataI = 0

                while (photoI < photoList.size && metadataI < metadataList.size) {
                    photoWithMetadataList +=
                        when {
                            photoList[photoI].path == metadataList[metadataI].path -> {
                                PhotoWithMetadata(photoList[photoI++], metadataList[metadataI++])
                            }
                            photoList[photoI].path < metadataList[metadataI].path -> {
                                PhotoWithMetadata(photoList[photoI++], null)
                            }
                            else -> {
                                PhotoWithMetadata(null, metadataList[metadataI++])
                            }
                        }
                }

                while (photoI < photoList.size) {
                    photoWithMetadataList += PhotoWithMetadata(photoList[photoI++], null)
                }

                while (metadataI < metadataList.size) {
                    photoWithMetadataList += PhotoWithMetadata(null, metadataList[metadataI++])
                }

                photoWithMetadataList
            }
        )

    private operator fun Photo.compareTo(metadata: PhotoMetadata): Int =
        path.compareTo(metadata.path)

    private operator fun PhotoMetadata.compareTo(photo: Photo): Int =
        path.compareTo(photo.path)
}