package com.viacheslav.bilyi.photofinder.di.module.data

import android.content.Context
import androidx.room.Room
import com.viacheslav.bilyi.photofinder.data.room.PhotoMetadataDatabase
import com.viacheslav.bilyi.photofinder.data.room.dao.PhotoMetadataDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {

    @Provides
    @Singleton
    fun providesPhotoMetadataDatabase(context: Context): PhotoMetadataDatabase =
        Room.databaseBuilder(
            context,
            PhotoMetadataDatabase::class.java,
            PhotoMetadataDatabase.NAME
        ).build()

    @Provides
    @Singleton
    fun providesPhotoMetadataDao(database: PhotoMetadataDatabase): PhotoMetadataDao =
        database.getPhotoMetadataDao()

}