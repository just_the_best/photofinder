package com.viacheslav.bilyi.photofinder.data.room.mapper

import com.viacheslav.bilyi.photofinder.data.room.entity.LabelInfoEntity
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.LabelInfo
import javax.inject.Inject

class LabelInfoEntityToDomainMapper @Inject constructor(

) : (LabelInfoEntity) -> LabelInfo {

    override fun invoke(entity: LabelInfoEntity) =
        LabelInfo(
            text = entity.text
        )
}