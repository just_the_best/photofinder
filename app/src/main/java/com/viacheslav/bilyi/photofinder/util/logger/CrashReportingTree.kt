package com.viacheslav.bilyi.photofinder.util.logger

import timber.log.Timber

class CrashReportingTree : Timber.Tree() {

    override fun log(
        priority: Int,
        tag: String?,
        message: String,
        throwable: Throwable?
    ) {
        //if (priority == Log.ERROR) {
        // send crash message to somewhere
        //}
    }
}

