package com.viacheslav.bilyi.photofinder.domain.entity.embedded

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LabelsInfo(
    val labels: List<LabelInfo>
) : Parcelable {

    @IgnoredOnParcel
    val isValid: Boolean = labels.isNotEmpty()

    companion object {
        val EMPTY = LabelsInfo(emptyList())
    }
}