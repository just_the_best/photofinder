package com.viacheslav.bilyi.photofinder.di.component

import android.content.Context
import com.viacheslav.bilyi.photofinder.Application
import com.viacheslav.bilyi.photofinder.di.module.ActivitiesProvider
import com.viacheslav.bilyi.photofinder.di.module.AppModule
import com.viacheslav.bilyi.photofinder.di.module.data.RepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,

        AppModule::class,
        ActivitiesProvider::class,
        RepositoryModule::class
    ]
)
interface AppComponent : AndroidInjector<Application> {

    @Component.Factory
    interface Factory {

        fun create(@BindsInstance context: Context): AppComponent
    }
}