package com.viacheslav.bilyi.photofinder.domain

import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import javax.inject.Inject

class GetPhotoMetadataDescriptionInteractor @Inject constructor(

) : (PhotoMetadata) -> String {

    override fun invoke(metadata: PhotoMetadata): String =
        StringBuilder()
            .append(metadata.textInfo)
            .append("\n")
            .appendIf(metadata.labelsInfo.isValid, metadata.labelsInfo.labels.map { it.text })
            .append("\n")
            .appendIf(metadata.containSmiling(), getSmilingDescription())
            .append("\n")
            .toString()
            .toLowerCase()

    companion object {

        private fun getSmilingDescription(): String = "smile"
    }
}

private fun PhotoMetadata.containSmiling(): Boolean =
    faceInfo
        .faceInfoList
        .map { it.isSmiling }
        .contains(true)

private fun StringBuilder.appendIf(
    enable: Boolean,
    stringList: List<String>
): StringBuilder =
    stringList
        .forEach {
            appendIf(enable, it)
            appendIf(enable, "\n")
        }
        .let { this }

private fun StringBuilder.appendIf(
    enable: Boolean,
    string: String
): StringBuilder =
    this.let {
        if (enable) append(string)
        else this
    }