package com.viacheslav.bilyi.photofinder.di.module.fragment

import com.viacheslav.bilyi.photofinder.di.scope.FragmentScope
import com.viacheslav.bilyi.photofinder.mvp.contract.PhotoPreviewContract
import com.viacheslav.bilyi.photofinder.mvp.presenter.PhotoPreviewPresenter
import dagger.Binds
import dagger.Module

@Module
interface PhotoPreviewFragmentModule {

    @Binds
    @FragmentScope
    fun bindPresenter(presenter: PhotoPreviewPresenter): PhotoPreviewContract.Presenter
}
