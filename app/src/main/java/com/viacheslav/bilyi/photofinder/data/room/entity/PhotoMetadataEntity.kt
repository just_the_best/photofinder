package com.viacheslav.bilyi.photofinder.data.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "photo_metadata"
)
data class PhotoMetadataEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "metadata_id")
    val metadataId: Long = 0,

    @ColumnInfo(name = "photo_path")
    val photoPath: String,

    @ColumnInfo(name = "content")
    val textContent: String?

)