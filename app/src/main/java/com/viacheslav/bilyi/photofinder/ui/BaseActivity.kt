package com.viacheslav.bilyi.photofinder.ui

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.viacheslav.bilyi.photofinder.mvp.BaseContract
import com.viacheslav.bilyi.photofinder.ui.navigation.BaseNavigator
import dagger.android.support.DaggerAppCompatActivity
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Inject

abstract class BaseActivity<V : BaseContract.View, P : BaseContract.Presenter<V>> :
    DaggerAppCompatActivity(),
    BaseContract.View {

    @Inject
    protected lateinit var presenter: P

    @Inject
    protected lateinit var navigatorHolder: NavigatorHolder

    @Inject
    protected lateinit var navigator: BaseNavigator

    @Inject
    protected lateinit var messageInterface: MessageInterface

    @get:LayoutRes
    protected abstract val layoutId: Int

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        presenter.attachView(this as V)
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    final override fun onError(throwable: Throwable) {
        messageInterface.showError(throwable)
    }

    override fun showProgress() {
        // TODO: show progress dialog
    }

    override fun hideProgress() {
        // TODO: hide progress dialog
    }
}