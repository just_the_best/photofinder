package com.viacheslav.bilyi.photofinder.data.room.mapper

import com.viacheslav.bilyi.photofinder.data.room.entity.FaceInfoEntity
import com.viacheslav.bilyi.photofinder.data.room.entity.LabelInfoEntity
import com.viacheslav.bilyi.photofinder.data.room.entity.RoomPhotoMetadata
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import javax.inject.Inject

class PhotoMetadataToRoomMapper @Inject constructor(
    private val photoMetadataToEntityMapper: PhotoMetadataToEntityMapper,
    private val faceInfoToEntityMapper: FaceInfoToEntityMapper,
    private val labelInfoToEntityMapper: LabelInfoToEntityMapper
) : (PhotoMetadata) -> RoomPhotoMetadata {

    override fun invoke(photoMetadata: PhotoMetadata) = RoomPhotoMetadata(
        metadataEntity = photoMetadataToEntityMapper(photoMetadata),
        faceInfoEntityList = getFaceInfoEntity(photoMetadata),
        labelInfoEntityList = getLabelInfoEntity(photoMetadata)
    )

    private fun getFaceInfoEntity(photoMetadata: PhotoMetadata): List<FaceInfoEntity> =
        photoMetadata
            .faceInfo
            .faceInfoList
            .map { faceInfoToEntityMapper(it, photoMetadata.path) }

    private fun getLabelInfoEntity(photoMetadata: PhotoMetadata): List<LabelInfoEntity> =
        photoMetadata
            .labelsInfo
            .labels
            .map { labelInfoToEntityMapper(it, photoMetadata.path) }
}