package com.viacheslav.bilyi.photofinder.domain

import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import com.viacheslav.bilyi.photofinder.domain.entity.PhotoMetadata
import io.reactivex.Single

interface PhotoMetadataRecognizer {

    fun recognizeMetadata(photo: Photo): Single<PhotoMetadata>
}