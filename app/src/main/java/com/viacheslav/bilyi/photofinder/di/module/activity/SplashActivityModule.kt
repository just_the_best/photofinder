package com.viacheslav.bilyi.photofinder.di.module.activity

import android.app.Activity
import com.viacheslav.bilyi.photofinder.di.scope.ActivityScope
import com.viacheslav.bilyi.photofinder.mvp.contract.SplashContract
import com.viacheslav.bilyi.photofinder.mvp.presenter.SplashPresenter
import com.viacheslav.bilyi.photofinder.ui.MessageInterface
import com.viacheslav.bilyi.photofinder.ui.activity.SplashActivity
import com.viacheslav.bilyi.photofinder.ui.dialog.AlertMessageInterface
import com.viacheslav.bilyi.photofinder.ui.navigation.BaseNavigator
import com.viacheslav.bilyi.photofinder.ui.navigation.SplashActivityNavigator
import dagger.Binds
import dagger.Module

@Module
interface SplashActivityModule {

    @Binds
    @ActivityScope
    fun bindAlertMessageInterface(messageInterface: AlertMessageInterface): MessageInterface

    @Binds
    @ActivityScope
    fun bindNavigator(navigator: SplashActivityNavigator): BaseNavigator

    @Binds
    @ActivityScope
    fun bindPresenter(presenter: SplashPresenter): SplashContract.Presenter

    @Binds
    @ActivityScope
    fun bindActivity(activity: SplashActivity): Activity
}