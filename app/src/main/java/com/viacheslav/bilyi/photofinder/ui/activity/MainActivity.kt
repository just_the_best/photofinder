package com.viacheslav.bilyi.photofinder.ui.activity

import android.os.Bundle
import com.viacheslav.bilyi.photofinder.R
import com.viacheslav.bilyi.photofinder.mvp.contract.MainContract
import com.viacheslav.bilyi.photofinder.ui.BaseActivity

class MainActivity :
    BaseActivity<MainContract.View, MainContract.Presenter>(),
    MainContract.View {

    override val layoutId = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.showSplashScreen()
    }
}