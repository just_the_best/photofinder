package com.viacheslav.bilyi.photofinder.mvp.contract

import com.viacheslav.bilyi.photofinder.mvp.BaseContract

interface MainContract {

    interface View : BaseContract.View

    interface Presenter : BaseContract.Presenter<View> {

        fun showSplashScreen()
    }
}