package com.viacheslav.bilyi.photofinder.data

import com.viacheslav.bilyi.photofinder.BuildConfig
import com.viacheslav.bilyi.photofinder.data.content.ContentResolverDao
import com.viacheslav.bilyi.photofinder.domain.PhotoRepository
import com.viacheslav.bilyi.photofinder.domain.entity.Photo
import io.reactivex.Single
import javax.inject.Inject

class PhotoRepositoryImpl @Inject constructor(
    private val contentResolverDao: ContentResolverDao
) : PhotoRepository {

    override fun getAll(): Single<List<Photo>> =
        contentResolverDao
            .getAll()

            .map { photos ->
                if (BuildConfig.DEBUG) photos.filter { it.path.contains("photoFinderDebug") }
                else photos
            }

    override fun getAllSortedByPhotoPath(): Single<List<Photo>> =
        getAll()
            .map { photos -> photos.sortedBy { it.path } }
}