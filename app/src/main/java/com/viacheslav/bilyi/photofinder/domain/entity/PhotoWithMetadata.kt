package com.viacheslav.bilyi.photofinder.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoWithMetadata(
    val photo: Photo?,
    val metadata: PhotoMetadata?
) : Parcelable

fun List<PhotoWithMetadata>.getPhotos(): List<Photo> =
    this.mapNotNull { it.photo }