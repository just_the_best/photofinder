package com.viacheslav.bilyi.photofinder.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.viacheslav.bilyi.photofinder.data.room.entity.FaceInfoEntity
import com.viacheslav.bilyi.photofinder.data.room.entity.LabelInfoEntity
import com.viacheslav.bilyi.photofinder.data.room.entity.PhotoMetadataEntity
import com.viacheslav.bilyi.photofinder.data.room.entity.RoomPhotoMetadata
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface PhotoMetadataDao {

    @Insert
    fun insertPhotoMetadataEntity(metadata: PhotoMetadataEntity): Completable

    @Insert
    fun insertFaceInfoEntity(metadata: FaceInfoEntity): Completable

    @Insert
    fun insertLabelInfoEntity(metadata: LabelInfoEntity): Completable

    @Transaction
    @Query("DELETE FROM photo_metadata WHERE photo_path = :photoPath")
    fun deleteByPath(photoPath: String): Completable

    @Transaction
    @Query("DELETE FROM photo_metadata")
    fun deleteAll(): Completable

    @Transaction
    @Query("SELECT * FROM photo_metadata WHERE photo_path = :photoPath")
    fun getByPath(photoPath: String): Single<RoomPhotoMetadata>

    @Transaction
    @Query("SELECT * FROM photo_metadata")
    fun getAll(): Single<List<RoomPhotoMetadata>>

    @Transaction
    @Query("SELECT * FROM photo_metadata ORDER BY photo_path ASC")
    fun getAllSorted(): Single<List<RoomPhotoMetadata>>

    fun insert(roomMetadata: RoomPhotoMetadata): Completable =
        saveFacesInfo(roomMetadata)
            .andThen(saveLabelsInfo(roomMetadata))
            .andThen(insertPhotoMetadataEntity(roomMetadata.metadataEntity))

    fun saveFacesInfo(metadata: RoomPhotoMetadata): Completable =
        Observable
            .fromIterable(metadata.faceInfoEntityList)
            .flatMapCompletable { insertFaceInfoEntity(it) }

    fun saveLabelsInfo(metadata: RoomPhotoMetadata): Completable =
        Observable
            .fromIterable(metadata.labelInfoEntityList)
            .flatMapCompletable { insertLabelInfoEntity(it) }

}