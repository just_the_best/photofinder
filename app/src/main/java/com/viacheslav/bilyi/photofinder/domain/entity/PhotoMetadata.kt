package com.viacheslav.bilyi.photofinder.domain.entity

import android.os.Parcelable
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.FacesInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.LabelsInfo
import com.viacheslav.bilyi.photofinder.domain.entity.embedded.TextInfo
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoMetadata(
    val path: String,
    val textInfo: TextInfo = TextInfo.EMPTY,
    val labelsInfo: LabelsInfo = LabelsInfo.EMPTY,
    val faceInfo: FacesInfo = FacesInfo.EMPTY
) : Parcelable {

    override fun toString() = "$path \n$textInfo \n$labelsInfo \n$faceInfo"
}