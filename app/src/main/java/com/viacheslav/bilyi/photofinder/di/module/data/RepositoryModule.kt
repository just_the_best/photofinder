package com.viacheslav.bilyi.photofinder.di.module.data

import com.viacheslav.bilyi.photofinder.data.PhotoMetadataRepositoryImpl
import com.viacheslav.bilyi.photofinder.data.PhotoRepositoryImpl
import com.viacheslav.bilyi.photofinder.domain.PhotoMetadataRepository
import com.viacheslav.bilyi.photofinder.domain.PhotoRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module(
    includes = [
        RoomModule::class,
        MlKitModule::class
    ]
)
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindsPhotoRepository(impl: PhotoRepositoryImpl): PhotoRepository

    @Binds
    @Singleton
    fun bindsPhotoMetadataRepository(impl: PhotoMetadataRepositoryImpl): PhotoMetadataRepository
}