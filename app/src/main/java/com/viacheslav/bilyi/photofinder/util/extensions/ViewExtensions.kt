package com.viacheslav.bilyi.photofinder.util.extensions

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

inline val View.layoutInflater get() = context.layoutInflater

fun ViewGroup.inflateView(@LayoutRes layoutId: Int, attachToRoot: Boolean = false): View =
    layoutInflater.inflate(layoutId, this, attachToRoot)
