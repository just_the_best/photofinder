package com.viacheslav.bilyi.photofinder.data.room.entity

import androidx.room.Embedded
import androidx.room.Relation

data class RoomPhotoMetadata(

    @Embedded
    val metadataEntity: PhotoMetadataEntity,

    @Relation(
        parentColumn = "photo_path",
        entityColumn = "parent_photo_path"
    )
    val faceInfoEntityList: List<FaceInfoEntity>,

    @Relation(
        parentColumn = "photo_path",
        entityColumn = "parent_photo_path"
    )
    val labelInfoEntityList: List<LabelInfoEntity>
)